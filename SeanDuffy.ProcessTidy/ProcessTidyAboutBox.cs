﻿using System;
using System.Reflection;
using System.Windows.Forms;

namespace SeanDuffy.ProcessTidy
{
   partial class AboutBoxProcessTidy : Form
   {
      public AboutBoxProcessTidy()
      {
         InitializeComponent();
         Text = @"About";
         labelProductName.Text = @"Dot Net Process Tidy";
         labelVersion.Text = String.Format("Version {0}", AssemblyVersion);
         labelCopyright.Text = AssemblyCopyright;
         labelCompanyName.Text = @"Sean Duffy";

         textBoxDescription.Text = string.Format("This utility will clean up any processes that match the following : '{0}'",
             string.Join("','", Form1.DevelopmentProcessPrefixes));
      }

      public override sealed string Text
      {
         get { return base.Text; }
         set { base.Text = value; }
      }

      #region Assembly Attribute Accessors

      public string AssemblyTitle
      {
         get
         {
            var attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            if (attributes.Length > 0)
            {
               var titleAttribute = (AssemblyTitleAttribute)attributes[0];
               if (titleAttribute.Title != "")
               {
                  return titleAttribute.Title;
               }
            }
            return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
         }
      }

      public string AssemblyVersion
      {
         get
         {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
         }
      }

      public string AssemblyDescription
      {
         get
         {
            var attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
            return attributes.Length == 0 ? string.Empty : ((AssemblyDescriptionAttribute)attributes[0]).Description;
         }
      }

      public string AssemblyProduct
      {
         get
         {
            var attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            return attributes.Length == 0 ? string.Empty : ((AssemblyProductAttribute)attributes[0]).Product;
         }
      }

      public string AssemblyCopyright
      {
         get
         {
            var attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
            return attributes.Length == 0 ? string.Empty : ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
         }
      }

      public string AssemblyCompany
      {
         get
         {
            var attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
            return attributes.Length == 0 ? string.Empty : ((AssemblyCompanyAttribute)attributes[0]).Company;
         }
      }
      #endregion
   }
}
