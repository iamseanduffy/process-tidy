﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Forms;
using System.Linq;

namespace SeanDuffy.ProcessTidy
{
    public partial class Form1 : Form
    {
        public static readonly string[] DevelopmentProcessPrefixes =
        {
            "webdev.",
            "wcfsvchost.",
            "iisexpress."
        };

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                return;
            }

            Hide();
            ShowInTaskbar = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Hide();
            ShowInTaskbar = false;
        }

        private void MenuItemExit_Click(object sender, EventArgs e)
        {
            notifyIconMain.Visible = false;
            Application.Exit();
        }

        private void MenuItemAbout_Click(object sender, EventArgs e)
        {
            var aboutDialog = new AboutBoxProcessTidy();
            aboutDialog.ShowDialog();
        }

        private void MenuItemTidy_Click(object sender, EventArgs e)
        {
            try
            {
                var developmentProcesses = GetDevelopmentProcesses();

                foreach (var developmentProcess in developmentProcesses)
                {
                    developmentProcess.Kill();
                }

                notifyIconMain.ShowBalloonTip(500,
                                              string.Empty,
                                              string.Format("{0} process{1} ended.",
                                                            developmentProcesses.Count,
                                                            developmentProcesses.Count == 1 ?
                                                                string.Empty :
                                                                "es"),
                                              ToolTipIcon.Info);

                NotificationAreaClean.RefreshTaskbarNotificationArea();
            }
            catch (Exception exception)
            {
                notifyIconMain.ShowBalloonTip(1000,
                                              string.Empty,
                                              exception.Message,
                                              ToolTipIcon.Error);
            }
        }

        private static IList<Process> GetDevelopmentProcesses()
        {
            return (from process in Process.GetProcesses()
                    from processPrefix in DevelopmentProcessPrefixes
                    where process.ProcessName.ToLower().StartsWith(processPrefix, true, CultureInfo.CurrentCulture)
                    select process).ToList();
        }


        private void notifyIconMain_MouseMove(object sender, MouseEventArgs e)
        {
            var processes = GetDevelopmentProcesses();
            var processCount = processes.Count;

            notifyIconMain.Text = string.Format("Dot Net Process Tidy\n{0} process{1} running",
                                                processCount,
                                                processCount == 1 ?
                                                    string.Empty :
                                                    "es");
        }
    }
}